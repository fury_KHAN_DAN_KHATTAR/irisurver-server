package com.digimpact.digdataservice.controller;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.digimpact.digdataservice.model.User;

@RestController
public class UserController {

	private static final Logger LOG = LoggerFactory.getLogger(UserController.class);
	
	@RequestMapping(value = "/user/{id}", method = RequestMethod.GET,headers="Accept=application/json")
	public User getUser(@PathVariable int id)
	{
		return new User();
	}
	
	@RequestMapping(value="/enroll", method = RequestMethod.POST)
	public @ResponseBody User enrolUser(@RequestBody User user)
	{
		
		LOG.info("Enrolling User : "+user.getFirstName());
		user.setId(new Random().nextInt());
		return user;
	}
}
