# README #

### How do I get local set up? ###

#### Assuming JDK 7 is already installed ####
 
* Install Maven 3 (https://maven.apache.org/download.cgi)
* Install Tomcat 7 (https://tomcat.apache.org/download-70.cgi)
* Install STS (https://spring.io/tools/sts)
* Start STS and install maven, git plugins (if required)
* It might require to setup a classpath variable M2_REPO (maven repository location, default location - ${user.home}/.m2/repository/)
* Clone the repository
* Import the project (choose General - existing projects into workspace)
* To run the application - choose Run on Server and follow the instructions to setup Tomcat 7
* The API can be tested with http://localhost:8080/digdataservice/user/1

* The enrol API can be tested with http://localhost:8080/digdataservice/enroll

![postman.jpg](https://bitbucket.org/repo/6G49xM/images/2300595283-postman.jpg)